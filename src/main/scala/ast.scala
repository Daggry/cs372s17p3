package edu.luc.cs.laufer.cs473.expressions.ast

/** An initial algebra of arithmetic expressions. */
\sealed trait Statement
sealed trait Expr extends Statement
case class Constant(value: Int) extends Expr
abstract class UnaryExpr(expr: Expr) extends Expr { require { expr != null } }
case class UMinus(expr: Expr) extends UnaryExpr(expr)
abstract class BinaryExpr(left: Expr, right: Expr) extends Expr { require { (left != null) && (right != null) } }
case class Plus(left: Expr, right: Expr) extends BinaryExpr(left, right)
case class Minus(left: Expr, right: Expr) extends BinaryExpr(left, right)
case class Times(left: Expr, right: Expr) extends BinaryExpr(left, right)
case class Div(left: Expr, right: Expr) extends BinaryExpr(left, right)
case class Mod(left: Expr, right: Expr) extends BinaryExpr(left, right)
case class Variable(name:String) extends Expr {
  require(name!=null)
}
case class Assignment (variable:Expr, value:Expr) extends Statement
case class Conditional (condition: Expr, a: Block, b: Option[Block]) extends Statement
case class Loop (condition: Expr, body: Block) extends Statement
case class Block(lines: Statement*) extends Statement {
  require(lines != null)
}

case class Struct(fields: String*) extends Statement
case class Select()
