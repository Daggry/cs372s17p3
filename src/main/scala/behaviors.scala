package edu.luc.cs.laufer.cs473.expressions

import ast._

object behaviors {

  def toFormattedString(prefix: String)(e: Statement): String = e match {
    case Constant(c) =>
      prefix + c.toString
    case UMinus(r) =>
      buildUnaryExprString(prefix, "UMinus", toFormattedString(prefix + INDENT)(r))
    case Plus(l, r) =>
      buildExprString(prefix, "Plus", toFormattedString(prefix + INDENT)(l), toFormattedString(prefix + INDENT)(r))
    case Minus(l, r) =>
      buildExprString(prefix, "Minus", toFormattedString(prefix + INDENT)(l), toFormattedString(prefix + INDENT)(r))
    case Times(l, r) =>
      buildExprString(prefix, "Times", toFormattedString(prefix + INDENT)(l), toFormattedString(prefix + INDENT)(r))
    case Div(l, r) =>
      buildExprString(prefix, "Div", toFormattedString(prefix + INDENT)(l), toFormattedString(prefix + INDENT)(r))
    case Mod(l, r) =>
      buildExprString(prefix, "Mod", toFormattedString(prefix + INDENT)(l), toFormattedString(prefix + INDENT)(r))
    case Variable(n) =>
      prefix + n
    case Assignment(identifier, value) =>
      buildExprString(prefix, "Assignment", toFormattedString(prefix + INDENT)(identifier), toFormattedString(prefix + INDENT)(value))
    case Block(statements @ _*) =>
      buildBlockString(prefix, "Block", statements.map(e => toFormattedString(prefix + INDENT)(e)))
    case Conditional(condition, trueTask, Some(falseTask)) =>
      buildConditionalString(prefix, "Conditional", toFormattedString(prefix + INDENT)(condition), toFormattedString(prefix + INDENT)(trueTask), toFormattedString(prefix + INDENT)(falseTask))
    case Loop(condition, loopBody) =>
      buildExprString(prefix, "Loop", toFormattedString(prefix + INDENT)(condition), toFormattedString(prefix + INDENT)(loopBody))
  }

  def toFormattedString(e: Statement): String = toFormattedString("")(e)

  def buildExprString(prefix: String, nodeString: String, leftString: String, rightString: String) = {
    val result = new StringBuilder(prefix)
    result.append(nodeString)
    result.append("(")
    result.append(EOL)
    result.append(leftString)
    result.append(", ")
    result.append(EOL)
    result.append(rightString)
    result.append(")")
    result.toString
  }

  def buildConditionalString(prefix: String, nodeString: String, condition: String, trueTask: String, falseTask: String): String = {
    val result = new StringBuilder(prefix)
    result.append(nodeString)
    result.append("(")
    result.append(EOL)
    result.append(condition)
    result.append(", ")
    result.append(EOL)
    result.append(trueTask)
    result.append(", ")
    result.append(falseTask)
    result.append(")")
    result.toString
  }

  def buildBlockString(prefix: String, nodeString: String, statements: Seq[String]) = {
    val result = new StringBuilder(prefix)
    result.append(nodeString)
    result.append("(")
    result.append(EOL)
    statements.map(e => result.append(e + EOL))
    result.append(")")
    result.toString
  }

  def buildUnaryExprString(prefix: String, nodeString: String, exprString: String) = {
    val result = new StringBuilder(prefix)
    result.append(nodeString)
    result.append("(")
    result.append(EOL)
    result.append(exprString)
    result.append(")")
    result.toString
  }

  val EOL = scala.util.Properties.lineSeparator
  val INDENT = ".."


}
