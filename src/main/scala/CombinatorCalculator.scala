package edu.luc.cs.laufer.cs473.expressions

import scala.util.parsing.combinator.JavaTokenParsers
import ast._
import edu.luc.cs.laufer.cs473.expressions.CombinatorParser.ident

import scala.annotation.meta.field

/*JavaTokenParsers extends scala.util.parsing.combinator.Parsers
* */
object CombinatorParser extends JavaTokenParsers {
  //    to avoid left recursion
  def main: Parser[Statement] = {
    rep(statement) ^^ {
      case list => {
        Block(list: _*)
      }
    }
  }

  /*  statement ::= expression ";" | assignment | conditional | loop | block */
  def statement: Parser[Statement] = (

    expr ~ ";" ^^ {
      case (e ~ _) => {
        e
      }
    }
      ||| assignment ^^ {
      case (a) => {
        a
      }
    }
      ||| conditional ^^ {
      case c => {
        c
      }
    }
      ||| loop ^^ {
      case l => {
        l
      }
    }
      ||| block ^^ {
      case b => {
        b
      }
    }
    )

  /** expr ::= term { { "+" | "-" } term }* */
  def expr: Parser[Expr] =
    term ~! rep(("+" | "-") ~ term) ^^ {
      case l ~ list => {
        list.foldLeft(l) {
          case (x, "+" ~ y) => {
            Plus(x, y)
          }
          case (x, "-" ~ y) => {
            Minus(x, y)
          }
        }
      }
    }

  /** term ::= factor { { "*" | "/" | "%" } factor }* */
  def term: Parser[Expr] =
    factor ~! rep(("*" | "/" | "%") ~ factor) ^^ {
      case l ~ list => {
        list.foldLeft(l) {
          case (x, "*" ~ y) => {
            Times(x, y)
          }
          case (x, "/" ~ y) => {
            Div(x, y)
          }
          case (x, "%" ~ y) => {
            Mod(x, y)
          }
        }
      }
    }

  /** factor ::= wholeNumber | "+" factor | "-" factor | "(" expr ")" */
  def factor: Parser[Expr] = (
    wholeNumber ^^ {
      case s => {
        Constant(s.toInt)
      }
    }
      ||| "+" ~ factor ^^ {
      case (_ ~ e) => {
        e
      }
    }
      ||| "-" ~ factor ^^ {
      case (_ ~ e) => {
        UMinus(e)
      }
    }
      ||| "(" ~ expr ~ ")" ^^ {
      case (_ ~ e ~ _) => {
        e
      }
    }

      ||| ident ^^ {
      case x => {
        Variable(x)
      }
    }

    )

  /* assignment ::= ident "=" expression ";" */
  def assignment: Parser[Statement] = {
    factor ~ "=" ~ expr ~ ";" ^^ {
      case (i ~ _ ~ v ~ _) => {
        Assignment(i, v)
      }
    }
  }

  /* conditional ::= "if" "(" expression ")" block [ "else" block ] */
  def conditional: Parser[Statement] = {
    "if" ~! "(" ~! expr ~! ")" ~! block ~ opt("else" ~! block) ^^ {
      case (_ ~ _ ~ e ~ _ ~ b ~ None) => {
        Conditional(e, b, None)
      }
      case (_ ~ _ ~ e ~ _ ~ b ~ Some(_ ~ eb)) => {
        Conditional(e, b, Some(eb))
      }
    }
  }

  /* loop ::= "while" "(" expression ")" block */
  def loop: Parser[Statement] = {
    "while" ~ "(" ~ expr ~ ")" ~ block ^^ {
      case (_ ~ _ ~ e ~ _ ~ b) => {
        Loop(e, b)
      }
    }
  }

  /*  block ::= "{" statement* "}" */
  def block: Parser[Block] = {
    "{" ~ rep(statement) ~ "}" ^^ {
      case (_ ~ e ~ _) => {
        Block(e: _*)
      }
    }
  }




  //  **** PROJECT 3c CODE ****

  //ASSIGNMENT TO AN EXISTING FIELD IN A STRUCTURE

  /* assignment  ::= ident { "." ident }* "=" expression ";" */

  def assignment1: Parser[Object] = {
    ident ^^ {
      case i => {
        ident ~ "{" ~ "." ~ ident ~ "}*" ~ "=" ~ expr ~ ";"
      }
    }
  }

  //FIELD SELECTION

  /* factor ::= ident { "." ident }* | number | "+" factor | "-" factor | "(" expr ")" | struct */

  def factor1: Parser[Object] = (
    ident ^^ {

      case i => {
        ident ~ "{" ~ "." ~ ident ~ "}*"
      }
    }
      ||| "+" ~ factor ^^ {
      case (_ ~ e) => {
        e
      }
    }
      ||| "-" ~ factor ^^ {
      case (_ ~ e) => {
        UMinus(e)
      }
    }
      ||| "(" ~ expr ~ ")" ^^ {
      case (_ ~ e ~ _) => {
        e
      }
    }

      ||| struct ^^ {
      case x => {
        struct
      }
    }

    )

  def field: Parser[Object] = {
    ident ^^ {
      case e => {
        ident ~ ":" ~ expr
      }
    }
  }

  def struct: Parser[Any] = "{" ~ "}" ^^ {
    case p => {
      "{" ~ "}"

    }
  } ||| "{" ~ statement ~ "{" ~ "," ~ statement ~ "}*" ~ "}" ^^ {
    case (_ ~ statement ~ _) => {
      statement

    }
  }

}

